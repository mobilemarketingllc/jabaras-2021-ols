var $ = jQuery;

function reloadFacet() {
    $(".facetwp-facet").each(function() {
        if (0 == $(this).children(".facet-inner").length) {
            var e = $(this).children(".facetwp-overflow").children(".facetwp-checkbox").length;
            $(this).children("div").wrapAll('<div class="facet-inner" />'), $(this).children(".facetwp-toggle:eq(0)").text("See " + e + " more")
        }
    })
}

function fr_click_outside(e, t, a, i) { $(document).click(function(n) { $(n.target).closest(e).length || (a && a(t) || !a && $(t).is(":visible")) && (i ? i(t) : $(t).hide()) }) }

function change_tab(e) {
    e.parent().find(".fl-tab-active").removeClass("fl-tab-active"), e.addClass("fl-tab-active");
    var t = e.index();
    e.parents(".fl-tabs").first().find(".fl-tabs-panel .fl-tabs-label").removeClass("fl-tab-active"), e.parents(".fl-tabs").first().find(".fl-tabs-panel .fl-tabs-panel-content").removeClass("fl-tab-active"), e.parents(".fl-tabs").first().find(".fl-tabs-panel:eq(" + t + ") .fl-tabs-panel-content").addClass("fl-tab-active")
}

function fr_parse_attr_data(e) { return e || (e = ""), "{" != e.substr(0, 1) && (e = "{" + e + "}"), $.parseJSON(e) }
$(document).ready(function() { $(".fr_toggle_box .box_content .handle").click(function() { $(".fr_toggle_box").toggleClass("active") }) }), $(window).ready(function() {
    var e;
    jQuery("#share_it_forward_trigger img").attr("onclick", "sws_syndication_Sif.openReviewsFrame()"), fr_add_filter("fr_slider_init", function(e, t) { return $(window).width() < 726 && t.is(".color_variations_slider") && (e.slidesToScroll = 3, e.slidesToShow = 3), e }), $().slick && ($(e = ".fr-slider").each(function() {
        var t = fr_parse_attr_data($(this).attr("data-fr"));

        function a(e, a) { 0 == a ? $(e).parents(".fr-slider").first().find(".arrow.prev").animate({ opacity: 0 }) : $(e).parents(".fr-slider").first().find(".arrow.prev").animate({ opacity: 1 }), a >= $(e).find(".slide").length - t.slidesToScroll ? $(e).parents(".fr-slider").first().find(".arrow.next").animate({ opacity: 0 }) : $(e).parents(".fr-slider").first().find(".arrow.next").animate({ opacity: 1 }) }
        t = fr_apply_filter("fr_slider_init", t = $.extend({ slidesToScroll: 1, slidesToShow: 1 }, t), [$(e)]), $(e).find(".slides").slick(t), $(e).find(".slides").on("beforeChange", function(e, t, i, n) { a(this, n) }), a($(e).find(".slides"), 0), $(e).addClass("init")
    }), $("body").on("click", ".fr-slider .prev", function(e) { e.preventDefault(), $(this).parents(".fr-slider").first().find(".slides").slick("slickPrev") }), $("body").on("click", ".fr-slider .next", function(e) { e.preventDefault(), $(this).parents(".fr-slider").first().find(".slides").slick("slickNext") })), $("body").on("click", "[data-fr-replace-src]", function(e) { e.preventDefault(), url = $(this).attr("data-src"), bkImg = "url('" + url + "')", $($(this).attr("data-fr-replace-src")).attr("src", bkImg) }), $("body").on("click", "[data-fr-replace-bg]", function(e) { e.preventDefault(), url = $(this).attr("data-background"), bkImg = "url('" + url + "')", $($(this).attr("data-fr-replace-bg")).css("background-image", bkImg) }), $(".fr_toggle_box .box_content").each(function() {
        $(this).show();
        var e = $(this).parents(".fr_toggle_box").first().data("dir"),
            t = $(this).outerHeight();
        "left" != e && "right" != e || (t = $(this).outerWidth()), $(this).css(e, -t)
    }), $(".fr_toggle_box .handle").click(function() {
        var e = $(this).parents(".fr_toggle_box").first(),
            t = e.find(".box_content").first(),
            a = e.data("dir");
        if (a || (a = "bottom"), data = {}, e.is(".active")) { var i = t.outerHeight(); "left" != a && "right" != a || (i = t.outerWidth()), data[a] = -i, t.stop().animate(data), e.removeClass("active"), e.find(".bg").fadeOut() } else data[a] = 0, t.stop().animate(data), e.addClass("active"), e.find(".bg").fadeIn()
    }), $(window).load(function() { setTimeout(function() { $.cookie("fr_toggle_box_opened") || ($(".fr_toggle_box .handle").click(), setTimeout(function() { $(".fr_toggle_box").is(".active") && $(".fr_toggle_box .handle").click() }, 7e3), $.cookie("fr_toggle_box_opened", 1, { expires: 365, path: "/" })) }, 1e3) });
    var t = 0;

    function a(e) { $(e).addClass("active"), $(e).find("ul").stop().animate({ "margin-right": 0 }) }

    function i(e) { $(e).removeClass("active"), $(e).find("ul").stop().animate({ "margin-right": -$(e).outerWidth() + $(e).find(".icon").first().outerWidth() }) }
    $("body").on("click", "[data-fr-link] a", function(e) { t++ }), $("body").on("click", "[data-fr-link]", function(e) {
        if (t <= 0) return window.location = $(this).attr("data-fr-link"), !1;
        t--
    }), $(".slider-menu").each(function() { $(this).height($(this).parent().height()) }), fr_click_outside(".slider-menu", ".slider-menu", function(e) { return $(e).is(".active") }, function(e) { $(e).is(".active") ? i(e) : a(e) }), $(".slider-menu").find(".icon").click(function() { $(this).parents(".slider-menu").is(".active") ? i($(this).parents(".slider-menu")) : a($(this).parents(".slider-menu")) }), $("body").on("click", "a[href^='#']", function(e) {
        var t = $(this).attr("href");
        $(t).length && $(t).is(".fr_popup") && (e.preventDefault(), $(t).is(".active") || function(e) {
            $(e).show(),
                function(e, t) {
                    t || (t = 0);
                    var a = $(window).scrollTop(),
                        i = $(window).height(),
                        n = $(e).outerHeight();
                    console.log(i, n, a, t), $(e).css("top", (i - n) / 2 + a + t)
                }($(e).find(".content")), $(e).addClass("active")
        }(t))
    }), $("body").on("click", ".fr_popup .close_popup", function(e) {
        e.preventDefault();
        var t = $(this).parents(".fr_popup");
        $(t).is(".active") && function(e) { $(e).removeClass("active"), $(e).hide() }(t)
    });
    var n = window.location.hash.substr(1);
    $(".fl-tabs-label").each(function() { if ($.trim($(this).text()).toLowerCase() == n.toLowerCase()) return console.log($(this)), change_tab($(this)), !1 }), $("footer .menu-item-has-children").click(function(e) { $(window).width() <= 768 && (e.preventDefault(), $(this).find(">ul").toggle()) }), $("header#djcustom-header .fl-photo").before("<a class='show_menu' href='#'><i class='fa fa-bars'></i></a>"), $("header#djcustom-header .fl-photo").before("<a class='close_search' href='#'><i class='fa fa-close'></i></a>"), $("header#djcustom-header").append("<div class='search_wrapper'>" + $(".fl-page-nav-search").html() + "</div>"), $("header#djcustom-header .search_wrapper>a").click(function(e) { e.preventDefault(), $(this).next().submit() }), $("header .fl-photo").before("<a href='#' class='search_mobile'><i class='fa fa-search'></i></a>"), $("header#djcustom-header .search_mobile").click(function(e) { e.preventDefault(), $("header#djcustom-header .search_wrapper").slideToggle() }), $(".show_menu").click(function(e) {
        e.preventDefault(), $(".sc_mega_menu").fadeIn(), $("header#djcustom-header .sc_mega_menu .mega-menu>ul").each(function() {
            $(this).parent().addClass("has_subitems"), $(".sc_mega_menu .fl-page-nav-wrap").prepend("<ul class='secondary_menu nav navbar-nav menu'></ul>"), $("header#djcustom-header .show_menu").hide(), $("header#djcustom-header .fl-photo").before("<a class='close_menu' href='#'><i class='fa fa-close'></i></a>");
            var e = $(this).parents(".sc_mega_menu .fl-page-nav-wrap");
            $("header#djcustom-header .close_menu").click(function(e) { e.preventDefault(), $(".sc_mega_menu").fadeOut(), $("header#djcustom-header .show_menu").show(), $(this).hide() }), $(this).parent().unbind().click(function(t) {
                var a = $(this);
                t.preventDefault(), e.animate({ "margin-left": -e.outerWidth() }, function() { $(".sc_mega_menu").is(".active") ? $(".sc_mega_menu").removeClass("active") : (console.log(1), $(".sc_mega_menu .secondary_menu").html("<li class='main_link'>" + $(a).html() + "</li>"), $(".sc_mega_menu").addClass("active"), $(".main_link>a").click(function(t) { t.preventDefault(), e.animate({ "margin-left": -e.outerWidth() }, function() { $(".sc_mega_menu .secondary_menu").html(""), $(".sc_mega_menu").removeClass("active"), e.animate({ "margin-left": 0 }) }) })), e.animate({ "margin-left": 0 }) })
            })
        })
    })
}), $(document).on("facetwp-refresh", function() { reloadFacet() }), $(document).on("facetwp-loaded", function() { $.each(FWP.settings.num_choices, function(e, t) { "" == $(".facetwp-facet-" + e).html() ? $(".facetwp-facet-" + e).parent().hide() : $(".facetwp-facet-" + e).parent().show() }), reloadFacet() }), reloadFacet();
var fr_filters = [];

function fr_add_filter(e, t) { void 0 === fr_filters[e = e] && (fr_filters[e] = []), fr_filters[e][fr_filters[e].length] = t }

function fr_apply_filter(e, t, a) {
    if (void 0 !== fr_filters[e])
        for (k in fr_filters[e]) {
            var i = [t].concat(a);
            t = fr_filters[e][k].apply(null, i)
        }
    return t
}


var agentfeesLabel = jQuery(".gf_progressbar_wrapper").detach();
jQuery(".gform_body").after(agentfeesLabel);
var current_page = jQuery("#gform_source_page_number_30").val();

if (current_page == "2" || current_page == "4") {
    jQuery(".gf_progressbar_wrapper .gf_progressbar .gf_progressbar_percentage span").html("66%");
    jQuery(".gf_progressbar_wrapper .gf_progressbar .gf_progressbar_percentage").attr("style", "width:66%");
} else if (current_page == "3") {
    jQuery(".gf_progressbar_wrapper .gf_progressbar .gf_progressbar_percentage span").html("33%");
    jQuery(".gf_progressbar_wrapper .gf_progressbar .gf_progressbar_percentage").attr("style", "width:33%");
} else if (current_page == "5") {
    jQuery(".gf_progressbar_wrapper .gf_progressbar .gf_progressbar_percentage span").html("100%");
    jQuery(".gf_progressbar_wrapper .gf_progressbar .gf_progressbar_percentage").attr("style", "width:100%");
}

jQuery('.gfield_radio input[type=radio]').bind("click", function() {

    if (jQuery(this).attr("name") == "input_2") {
        if (jQuery(this).val() == "Poor" || jQuery(this).val() == "Fair" || jQuery(this).val() == "Average")
            jQuery("#gform_target_page_number_30").val("3");
        jQuery("#gform_30").trigger("submit", [true]);
        if (jQuery(this).val() == "Good" || jQuery(this).val() == "Excellent")
            jQuery("#gform_target_page_number_30").val("2");
        jQuery("#gform_30").trigger("submit", [true]);
    }
    if (jQuery(this).attr("name") == "input_7") {
        if (jQuery(this).val() == "Proceed to Leave Your Feedback.")
            jQuery("#gform_target_page_number_30").val("4");
        jQuery("#gform_30").trigger("submit", [true]);
    }

});

if (window.location.hash == '#gf_30') {
    setTimeout(function() {

        if (jQuery('#input_30_32').val() != "") {

            jQuery('.locationvalue').html(jQuery('#input_30_32').val());
        }

        var location_val = $("#input_30_32 option:selected").text();
        if (location_val == 'Jabaras Carpet Outlet') {

            $('#label_30_28_0 .link>a').attr('href', "https://www.google.com/search?q=jabaras+flooring&oq=jabaras+flooring&aqs=chrome..69i57j0l4j69i60l3.5215j0j7&sourceid=chrome&ie=UTF-8#lrd=0x87bae236d34d1135:0xee9782b3eab057f5,3,,,").attr('target', '_blank');
            $('#label_30_28_1 .link>a').attr('href', "https://www.yelp.com/biz/jabarass-wichita-2?osq=jabara%27s+flooring").attr('target', '_blank');

        } else if (location_val == 'Jabaras Carpet Galerie') {

            $('#label_30_28_0 .link>a').attr('href', "https://www.google.com/search?q=jabaras+flooring&oq=jabaras+flooring&aqs=chrome..69i57j0l4j69i60l3.5215j0j7&sourceid=chrome&ie=UTF-8#lrd=0x87bae236d34d1135:0xee9782b3eab057f5,3,,,").attr('target', '_blank');
            $('#label_30_28_1 .link>a').attr('href', "https://www.yelp.com/biz/jabarass-wichita-2?osq=jabara%27s+flooring").attr('target', '_blank');

        } else if (location_val == 'Jabaras Home Improvement') {

            $('#label_30_28_0 .link>a').attr('href', "https://www.google.com/search?q=jabaras+flooring&oq=jabaras+flooring&aqs=chrome..69i57j0l4j69i60l3.5215j0j7&sourceid=chrome&ie=UTF-8#lrd=0x87bae236d34d1135:0xee9782b3eab057f5,3,,,").attr('target', '_blank');
            $('#label_30_28_1 .link>a').attr('href', "https://www.yelp.com/biz/jabarass-wichita-2?osq=jabara%27s+flooring").attr('target', '_blank');

        }
    }, 500);
}



jQuery( document ).ready(function() {
   jQuery('.navbar-toggle').on('click', function(e) {
      jQuery('.fl-page-nav > .fl-page-nav-collapse').toggleClass("in"); 
      e.preventDefault();
    });

    gform.addFilter( 'gform_datepicker_options_pre_init', function( optionsObj, formId, fieldId ) {
        if ( formId == 31 && fieldId == 19 ) {
            optionsObj.firstDay = 1;
            optionsObj.beforeShowDay = function(date) {
                var day = date.getDay();
                return [day != 0,''];
            }
        }
        return optionsObj;
    });
});
;