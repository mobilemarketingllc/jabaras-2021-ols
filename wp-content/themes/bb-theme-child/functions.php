<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    // wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.min.js","","",1);
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    // wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.css");
    wp_enqueue_style("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.css");
    //wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
});

 
// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'gallery-menu' => __( 'Gallery Menu' ),
            'top-bar-menu' => __( 'Top Bar Menu' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

 


//Search only products
function searchfilter($query) {

    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('carpeting','hardwood','laminate','luxury_vinyl_tile','solid_wpc_waterproof','tile'));
    }

    return $query;
}

add_filter('pre_get_posts','searchfilter');

// Facetwp results
add_filter( 'facetwp_result_count', function( $output, $params ) {
    //$output = $params['lower'] . '-' . $params['upper'] . ' of ' . $params['total'] . ' results';
    $output =  $params['total'] . ' Products';
    return $output;
}, 10, 2 );
// Facetwp results pager
function my_facetwp_pager_html( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    if ( $page > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '"><span class="pager-arrow"><</span></a>';
    }
    $output .= '<span class="pager-text">page ' . $page . ' of ' . $total_pages . '</span>';
    if ( $page < $total_pages && $total_pages > 1 ) {
        $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '"><span class="pager-arrow">></span></a>';
    }
    return $output;
}

add_filter( 'facetwp_pager_html', 'my_facetwp_pager_html', 10, 2 );

// Add to functions.php
function fwp_pagenavi_support() {
?>
<script>
(function($) {
		
    $(document).on('facetwp-refresh', function() {		
        if (! FWP.loaded) {			
			$(document).on('change', '.facetwp-per-page select.facetwp-per-page-select', function(e) { 				
				FWP.extras.per_page =  parseInt($(this).val());
               // FWP.soft_refresh = false;
		        FWP.refresh();
			
			});
            $(document).on('click', '.facetwp-pager a.facetwp-page', function(e) {			
                e.preventDefault();				
                var matches = $(this).attr('data-page').match(/\/page\/(\d+)/);
                if (null != matches) {
                    FWP.paged = parseInt(matches[1]);
					console.log(FWP.paged);
                }
				FWP.paged = parseInt($(this).attr('data-page'));
                //FWP.soft_refresh = false;
		        FWP.refresh();				
            });
        }
    });
})(jQuery);
</script>
<?php
}
add_action( 'wp_head', 'fwp_pagenavi_support', 50 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


acf_add_options_page( array("page_title"=>"Theme Settings") );



function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
        $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
              if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
              }else{
                return wp_get_attachment_image($id,$size,0,$attr);
              }
        }else if($url){
          return $image_url;
        }
    }
}
/*
function fwp_api_check_permissions() {
    return current_user_can( 'manage_options' );
}
add_filter( 'facetwp_api_can_access', 'fwp_api_check_permissions' );
*/

remove_action( 'wp_head', 'feed_links_extra', 3 );
 
// shortcode to show H1 google keyword fields
function new_google_keyword() 
{
       return $google_keyword = '<h1 class="googlekeyword">SAVE UP TO $500 ON FLOORING*<h1>';
}
add_shortcode('google_keyword_code', 'new_google_keyword');


// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
			   font-size:2.5em !important;
		   }
      </style>  
   <?php    
}

// Remove query string from static content
function _remove_script_version( $src ){ 
//if(strpos($src,'https://fonts.googleapis') === true) {
	$parts = explode( '?', $src );
	return $parts[0]; 
//}
//return $src;
} 
add_filter( 'script_loader_src', '_remove_script_version', 15, 2 ); 
add_filter( 'style_loader_src', '_remove_script_version', 15, 2 );

function new_year_number() 
{
	return $new_year = date('Y');
}
add_shortcode('year_code_4', 'new_year_number');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

function index_is_roomvo_featured( $params, $class ) {
    if ( 'roomvo' == $params['facet_name'] ) { // the facet is named "is_featured"
        $value =  $params['facet_value']; 
        if($value == '0' ){$display_value = 'Not Enabled';}
        if($value == '1' ){$display_value = 'Enabled';}
        if($value == 'Yes' ){$display_value = 'Enabled';}
        if($value == 'No' ){$display_value = 'Not Enabled';}
        $params['facet_display_value'] = $display_value;
    }
    return $params;
}
add_filter( 'facetwp_index_row', 'index_is_roomvo_featured', 10, 2 );

wp_clear_scheduled_hook( '404_redirection_log_cronjob' );
 wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );
if (!wp_next_scheduled('bmg_404_redirection_301_log_cronjob')) {
    
        wp_schedule_event( time() +  17800, 'daily', 'bmg_404_redirection_301_log_cronjob');
}

add_action( 'bmg_404_redirection_301_log_cronjob', 'bmg_custom_404_redirect_hook' ); 

//add_action( '404_redirection_log_cronjob', 'custom_404_redirect_hook' ); 


function bmg_check_404($url) {
   $headers=get_headers($url, 1);
   if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
}

 // custom 301 redirects from  404 logs table
 function bmg_custom_404_redirect_hook(){
    global $wpdb;    
    write_log('in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = bmg_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

           
            $destination_url_carpet = '/flooring/carpet/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_carpet,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = bmg_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_hardwood = '/flooring/hardwood/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_hardwood,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = bmg_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_laminate = '/flooring/laminate/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_laminate,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = bmg_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            if (strpos($row_404->url,'luxury-vinyl') !== false ){
                $destination_url_lvt = '/flooring/luxury-vinyl/products/';
            }elseif(strpos($row_404->url,'vinyl') !== false){
                $destination_url_lvt = '/flooring/luxury-vinyl/products/';
            }
            
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_lvt,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'ceramic-tile') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = bmg_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url_tile = '/flooring/ceramic-tile/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_tile,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'tile 301 added ');
        }

       }  
    }

 }
 //add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   
}
add_filter( 'auto_update_plugin', '__return_false' );

add_filter("gform_field_validation_31_18", "validate_date", 10, 4);
function validate_date($result, $value, $form, $field){
    // var_dump($value);die();
    if($value[0] !="" && $value[1] !=""){
        $input_time = strtotime( @$value[0].":".@$value[1]." ".@$value[2] );
        $max_str = "04:00 pm";
        $min_str = "08:00 am";
        $max_time = strtotime($max_str);
        $min_time = strtotime($min_str);
        if ($input_time < $min_time || $input_time > $max_time ) {
            $result["is_valid"] = false;
            $result["message"] = "Please select a time between ".$min_str." and ".$max_str;
        }
    }    
    return $result;
}

add_filter( 'gform_submit_button_39', 'add_paragraph_below_submit', 10, 2 );
function add_paragraph_below_submit( $button, $form ) {
    $button .= '<p class="button_msg">Please ensure the <strong>"Submit Payment"</strong> button can be clicked only once.</p>';
    $button .= '<p class="payment_msg">After click "Submit Payment" button wait while your payment is in processed. Please do not close or reload page.</p>';
    return $button;
}

// The following declaration targets all fields in form 6
// add_filter( 'gform_validation_39', 'custom_validation' );
// function custom_validation( $validation_result ) {
//     $form = $validation_result['form'];

//     //supposing we don't want input 1 to be a value of 86
//     if ( rgpost( 'input_1' ) == 86 ) {
  
//         // set the form validation to false
//         $validation_result['is_valid'] = false;
  
//         //finding Field with ID of 1 and marking it as failed validation
//         foreach( $form['fields'] as &$field ) {
  
//             //NOTE: replace 1 with the field you would like to validate
//             if ( $field->id == '1' ) {
//                 $field->failed_validation = true;
//                 $field->validation_message = 'This field is invalid!';
//                 break;
//             }
//         }
  
//     }
  
//     //Assign modified $form object back to the validation result
//     $validation_result['form'] = $form;
//     return $validation_result;
  
// }

add_shortcode( 'printshawcouponcode', 'printshawcouponcode' );
function printshawcouponcode(){
    ob_start();
    ?>
    <input type="button" class="print_coupon_btn btn fl-button" value="PRINT COUPON" onclick="PrintDiv();" />
    <script type="text/javascript">     
        function PrintDiv() {   
            var printContents = document.getElementById('printCouponBlock').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
        }
    </script>
 <?php
 $output = ob_get_contents();
 ob_end_clean();
 return $output;
}
add_shortcode( 'thankyoushawcouponcode', 'thankyoushawcouponcode_func' );
function thankyoushawcouponcode_func($arg) {

    $url = 'https://sso.mm-api.agency/oauth/token?grant_type=client_credentials&client_id=wordpress@canwilltech.com&client_secret=NYESqMvXRHCpIsWHpHGkTnWm';
    
    $authresponse = wp_remote_post($url,array(
                    'method' => 'POST',
                    'timeout' => 45,
                    'redirection' => 5,
                    'headers'   => array('Content-Type' => 'application/json; charset=utf-8'),
                    'httpversion' => '1.0',
                    'blocking' => true,               
                    'cookies' => array(),
                    'body' =>''
                    )
                );

    $responceData = json_decode( $authresponse['body'], true);
   // write_log($responceData['access_token']);

    if(isset($responceData['error'])){

        echo $responceData['error'];

    }else if(isset($responceData['access_token'])){

        if($_GET['entry'] != ''){

                $entry_id = $_GET['entry'];
                $entry = GFAPI::get_entry( $entry_id );

               // write_log($entry);

                $url_coupon = 'https://crm.mm-api.agency/coupon/code?email='.$entry[3].'&locationCode='.$entry[22].'&firstName='.$entry[1].'&lastName='.$entry[2].'&phone='.$_POST['input_4'];
                
                $headers = array('authorization'=>"bearer ".$responceData['access_token']);
               
                $response = wp_remote_get($url_coupon,array(
                    'method' => 'GET',
                    'timeout' => 45,
                    'redirection' => 5,
                    'headers' =>$headers,
                    'httpversion' => '1.0',
                    'blocking' => true,               
                    'cookies' => array(),
                    'body' =>''
                    )
                );
                // write_log('Coupon code response');
              // write_log($response['body']);

                $coupon_response = json_decode( $response['body'], true);
                $coupon_code = strtoupper($coupon_response['couponCode']);

                gform_add_meta($entry['id'], 'couponCode', $coupon_code, $entry['form_id']);
                return $coupon_code;
        }
    }    
}
