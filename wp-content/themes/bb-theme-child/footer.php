		<?php do_action('fl_content_close'); ?>
	
	</div><!-- .fl-page-content -->
	<?php 
		
	do_action('fl_after_content'); 
	
	if ( FLTheme::has_footer() ) :
	
	?>
	<footer class="fl-page-footer-wrap" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
		<?php 
			
		do_action('fl_footer_wrap_open');
		do_action('fl_before_footer_widgets');

		
		FLTheme::footer_widgets();
		
		do_action('fl_after_footer_widgets');
		do_action('fl_before_footer');
		
		FLTheme::footer();
		
		do_action('fl_after_footer');
		do_action('fl_footer_wrap_close');
		
		?>
	</footer>
	<?php endif; ?>
	<?php do_action('fl_page_close'); ?>
</div><!-- .fl-page -->
<?php if(@$_GET['get']=='ceramic'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( "#fl-tabs-58e20ef62aa72-label-3" ).addClass( "fl-tab-active" );
	jQuery( "#fl-tabs-58e20ef62aa72-label-0" ).removeClass( "fl-tab-active" );	
	jQuery( "#fl-tabs-58e20ef62aa72-panel-3" ).addClass( "fl-tab-active" );
	jQuery( "#fl-tabs-58e20ef62aa72-panel-0" ).removeClass( "fl-tab-active" );
	 document.getElementById('fl-tabs-58e20ef62aa72-label-3').setAttribute('aria-expanded', 'true');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-0').setAttribute('aria-expanded', 'false');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-3').setAttribute('aria-selected', 'true');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-0').setAttribute('aria-selected', 'false'); 
  });
</script>
<?php } else if(@$_GET['get']=='laminate'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( "#fl-tabs-58e20ef62aa72-label-2" ).addClass( "fl-tab-active" );
	jQuery( "#fl-tabs-58e20ef62aa72-label-0" ).removeClass( "fl-tab-active" );	
	jQuery( "#fl-tabs-58e20ef62aa72-panel-2" ).addClass( "fl-tab-active" );
	jQuery( "#fl-tabs-58e20ef62aa72-panel-0" ).removeClass( "fl-tab-active" );
	 document.getElementById('fl-tabs-58e20ef62aa72-label-2').setAttribute('aria-expanded', 'true');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-0').setAttribute('aria-expanded', 'false');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-2').setAttribute('aria-selected', 'true');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-0').setAttribute('aria-selected', 'false'); 
  });
</script>
<?php } else if(@$_GET['get']=='hardwood'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( "#fl-tabs-58e20ef62aa72-label-1" ).addClass( "fl-tab-active" );
	jQuery( "#fl-tabs-58e20ef62aa72-label-0" ).removeClass( "fl-tab-active" );	
	jQuery( "#fl-tabs-58e20ef62aa72-panel-1" ).addClass( "fl-tab-active" );
	jQuery( "#fl-tabs-58e20ef62aa72-panel-0" ).removeClass( "fl-tab-active" );
	 document.getElementById('fl-tabs-58e20ef62aa72-label-1').setAttribute('aria-expanded', 'true');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-0').setAttribute('aria-expanded', 'false');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-1').setAttribute('aria-selected', 'true');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-0').setAttribute('aria-selected', 'false'); 
  });
</script>
<?php } else if(@$_GET['get']=='lvt'){ ?>
<script>
 jQuery(document).ready(function(){
    jQuery( "#fl-tabs-58e20ef62aa72-label-4" ).addClass( "fl-tab-active" );
	jQuery( "#fl-tabs-58e20ef62aa72-label-0" ).removeClass( "fl-tab-active" );	
	jQuery( "#fl-tabs-58e20ef62aa72-panel-4" ).addClass( "fl-tab-active" );
	jQuery( "#fl-tabs-58e20ef62aa72-panel-0" ).removeClass( "fl-tab-active" );
	 document.getElementById('fl-tabs-58e20ef62aa72-label-4').setAttribute('aria-expanded', 'true');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-0').setAttribute('aria-expanded', 'false');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-4').setAttribute('aria-selected', 'true');
	 document.getElementById('fl-tabs-58e20ef62aa72-label-0').setAttribute('aria-selected', 'false'); 
  });
</script>
<?php } ?>
<?php 
	
wp_footer(); 

do_action('fl_body_close');

FLTheme::footer_code();

?>
</body>
</html>