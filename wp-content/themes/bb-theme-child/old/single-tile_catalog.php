	<?php get_header(); ?>
<!-- <div class="container">
    <div class="row">
    <div class="breadcrumbs" typeof="BreadcrumbList" property="itemListElement" vocab="https://schema.org/">
		  <a href="/">Home</a> &raquo; <a href="/flooring/">Flooring</a> &raquo; <a href="/flooring/ceramic-tile/">Tile</a> &raquo; <a href="/flooring/ceramic-tile/products/">Tile Products</a> &raquo; <?php the_title(); ?>
    </div>
        </div>
</div> -->

		<div class="fl-content product<?php //FLTheme::content_class(); ?>">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				<?php get_template_part('content', 'single-product'); ?>
			<?php endwhile; endif; ?>
		</div>
		<?php //FLTheme::sidebar('right'); ?>

<?php get_footer(); ?>