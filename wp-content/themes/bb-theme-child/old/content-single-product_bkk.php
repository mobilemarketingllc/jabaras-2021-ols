<?php
global $post;
$flooringtype = $post->post_type; 
$brand = get_field('brand') ;
$itemImage = get_field('swatch_image_link') ? get_field('swatch_image_link'):"http://placehold.it/600x400?text=No+Image"; 
if(strpos($itemImage , 's7.shawimg.com') !== false){
	if(strpos($itemImage , 'http') === false){ 
		$itemImage = "http://" . $itemImage;
	}
	$class = "";
}else{
	if(strpos($itemImage , 'http') === false){ 
		$itemImage = "https://" . $itemImage;
	}
}							  
$image= "https://mobilem.liquifire.com/mobilem?source=url[".$itemImage . "]&scale=size[600x400]&sink";
		
?>
<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Product">

	<header class="fl-post-header">

		<?php //FLTheme::post_top_meta(); ?>
	</header><!-- .fl-post-header -->

	<div class="fl-post-content clearfix grey-back" itemprop="text">

        <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 product-swatch">

                      <div class="img-responsive toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;background-position:bottom">
						<img src="<?php echo $image; ?>" class="img-responsive toggle-image" alt="<?php the_title_attribute(); ?>" />
					</div>

					<?php if(get_field('gallery_images')){ ?>
					<div class="toggle-image-thumbnails">
					<a href="#" data-background="<?php echo $image; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $image; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
						<?php
                       // check if the repeater field has rows of data
						if( have_rows('gallery_images') ):
                         
							// loop through the rows of data
						while ( have_rows('gallery_images') ) : the_row();
							 $room_image = get_sub_field('gallery_uploadimage');
							
							if(strpos($room_image , 's7.shawimg.com') !== false){
								if(strpos($room_image , 'http') === false){ 
									$room_image = "http://" . $room_image;
								}
								$room_image = $room_image ;
							}else{
								if(strpos($room_image , 'http') === false){ 
									$room_image = "https://" . $room_image;
								}
								$room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[600x400]&sink";
							}	
						?><a href="#" data-background="<?php echo $room_image; ?>" data-fr-replace-bg=".toggle-image" style="background-image:url('<?php echo $room_image; ?>');background-size: cover;" title="<?php the_title_attribute(); ?>"></a>
						<?php
							 endwhile;
							endif;
						?>
					</div>
					<?php } ?>

            </div>
			
            <div class="col-md-6 col-sm-6 product-box">

                <?php get_template_part('includes/product-brand-logos'); ?>

                <?php if(get_field('parent_collection')) { ?>
                <h2><?php the_field('parent_collection'); ?> </h2>
                <?php } ?>


<!--                 <h2><?php the_field('collection'); ?></h2> -->
                <h1 class="fl-post-title" itemprop="name">
                   <?php the_field('collection'); ?>
                </h1>
				<h4 class="fl-post-title" itemprop="name">
                   <?php the_field('color'); ?>
                </h4>

                <div class="product-colors">
                    <?php
					
                    	$value = get_post_meta($post->ID, 'collection', true);
					if(trim($value) !=""){
						$key =  "collection";
					
                    $args = array(
                        'post_type'      => $flooringtype,
                        'posts_per_page' => 10,
                        'post_status'    => 'publish',
                        'meta_query'     => array(
                            array(
                                'key'     => $key,
                                'value'   => $value,
                                'compare' => '='
                            )
                        )
                    );
                   
                    $the_query = new WP_Query( $args );
					
				    ?>
                    <ul>
						
                        <li class="found"><?php  echo count($the_query->posts); ?></li>

					
                        <li class="colors">Colors<br/>Available</li>
                    </ul>
				<?php } ?>
                </div>

                <a href="/flooring-coupon/?keyword=<?php echo $_COOKIE['keyword']; ?>&brand=<?php echo get_field('manufacturer');?>" class="fl-button" role="button" style="width: auto;">
                    <span class="fl-button-text">GET COUPON</span>
                </a>
<br />
                <a href="/contact-us/" class="fl-button" role="button" style="width: auto;">
                    <span class="fl-button-text">CONTACT US</span>
                </a>
                <br />
                <a href="/flooring-financing/">GET FINANCING ></a>
				<br />
				<br />
                <a href="/flooring-services/schedule-appointment/">SCHEDULE APPOINTMENT ></a>
                <div class="product-atts">
                </div>
            </div>
        </div>
		<?php if(trim($value) !=""){ ?>
        <?php get_template_part('includes/product-color-slider'); ?>
		<?php } ?>
</div>



    </div><!-- .fl-post-content -->
<div class="container">
    <?php get_template_part('includes/product-attributes'); ?>
</div>

</article>
<!-- .fl-post -->