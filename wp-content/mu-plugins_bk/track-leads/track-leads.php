<?php

// Plugin Name: MM Track Leads
// Description: Sends leads to the client CRM, according to the CLIENT_CODE and SITE_CODE that are defined in wp-config.php.
// CLIENT_CODE and SITE_CODE must match what is defined in the clients API for this client.

add_action('gform_after_submission', 'createLeadsAfterFormSubmission', 10, 2);
add_action('wpcf7_before_send_mail', 'createLeadsAfterCF7Submission', 5, 2);

function createLeadsAfterFormSubmission($entry, $form) {
    write_log('tracking lead for ' . $form['title']);
    $lead = array();
    $lead['source'] = 'Website';
    $lead['form'] = $form['title'];
    $lead['entry'] = $entry;

    $fields = array();

    foreach ($form['fields'] as $f) {
        $label = $f->label;
        $id = $f->id;

        $fields[] = array(
            'id' => (string) $f->id,
            'type' => $f->type,
            'label' => $f->label
        );

        if (isset($f->inputs)) {
            foreach ($f->inputs as $input) {
                $fields[] = $input;
            }
        }

        if (array_key_exists($id, $entry)) {
            $value = $entry[$id];
            createLeadsSetProperty($lead, $label, $value);
        } else {
            write_log($id . ' not found in form ' . $lead['form']);
        }
    }

    $lead['fields'] = $fields;
    post($lead);
}


function createLeadsAfterCF7Submission($form) {
    $lead = array();
    $lead['source'] = 'Website';
    $lead['form'] = $form->name();

    $submission = WPCF7_Submission::get_instance();
    $formData = $submission->get_posted_data();

    foreach ($formData as $label => $value) {
        createLeadsSetProperty($lead, $label, $value);
    }

    $firstName = $lead['firstName'];
    $lastName = $lead['lastName'];
    if ($lastName && !$firstName) {
        $split = explode(' ', $lastName);
        if (count($split) > 1) {
            if (count($split) === 2) {
                $firstName = $split[0];
                $lastName = $split[1];
            } else if (count($split) > 2) {
                $firstName = $split[0];
                $lastName = implode(' ', array_slice($split, 1));
            }
            $lead['firstName'] = $firstName;
            $lead['lastName'] = $lastName;
        }
    }

    post($lead);
}


/**
 * @param array $lead
 */
function post(array $lead) {
    $ch = curl_init();
    $query = http_build_query($lead);
    $clientCode = CLIENT_CODE;
    $siteCode = SITE_CODE;

    $url = "https://crm.mm-api.agency/$clientCode/$siteCode/lead";
    $json = json_encode($lead);
    write_log("sending $json to $url");

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($json)));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    if (curl_exec($ch) === false) {
        write_log(curl_error($ch));
    }

    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ('200' != $status) {
        mail('steve@mobile-marketing.agency', 'failed lead submission', $status . ' for ' . $url . '\n\n' . $json);
        write_log($status . ' for ' . $url . '\n' . $json);
    };

    curl_close($ch);
}


function createLeadsSetProperty(array &$lead, $label, $value) {
    write_log('setting ' . $label . ' to ' . $value);

    if (null !== $value) {
        $lc_label = strtolower($label);
        $lc_label = preg_replace('/\*/', '', $lc_label);
        $lc_label = trim($lc_label);
        $propName = null;

        switch ($lc_label) {
            case 'first name':
                $propName = 'firstName';
                break;

            case 'first-name':
                $propName = 'firstName';
                break;

            case 'last name':
                $propName = 'lastName';
                break;

            case 'last-name':
                $propName = 'lastName';
                break;

            case 'your name':
                $propName = 'lastName';
                break;

            case 'name':
                $propName = 'lastName';
                break;

            case 'email address':
                $propName = 'email';
                break;

            case 'email-address':
                $propName = 'email';
                break;

            case 'your email':
                $propName = 'email';
                break;

            case 'email':
                $propName = 'email';
                break;

            case 'phone number':
                $propName = 'phone';
                break;

            case 'phone':
                $propName = 'phone';
                break;

            case 'mobile':
                $propName = 'phone';
                break;

            case 'mobile phone':
                $propName = 'phone';
                break;

            case 'mobile number':
                $propName = 'phone';
                break;

            case 'mobile phone number':
                $propName = 'phone';
                break;

            case 'your mobile phone number':
                $propName = 'phone';
                break;

            case 'mobile-number':
                $propName = 'phone';
                break;

            case 'zip code':
                $propName = 'postalCode';
                break;

            case 'zip-code':
                $propName = 'postalCode';
                break;

            case 'zip':
                $propName = 'postalCode';
                break;

            case 'your postal code':
                $propName = 'postalCode';
                break;

            case 'postal code':
                $propName = 'postalCode';
                break;

            case 'location':
                $propName = 'location';
                break;

            case 'choose location':
                $propName = 'location';
                break;

            case 'choose a location':
                $propName = 'location';
                break;

            case 'preferred location':
                $propName = 'location';
                break;

            case 'mobile-opt-in':
                $propName = 'optIn';
                break;

            case 'terms-agreed':
                $propName = 'agreed';
                break;

            case 'comments':
                $propName = 'comments';
                break;

            case 'date of estimate':
                $propName = 'estimateDate';
                break;

            case 'preferred date of estimate':
                $propName = 'estimateDate';
                break;

            case 'preferred_day':
                $propName = 'estimateDate';
                break;

            case 'preferred_date':
                $propName = 'estimateDate';
                break;

            case 'preferred time of estimate':
                $propName = 'estimateTime';
                break;

            case 'preferred_time':
                $propName = 'estimateTime';
                break;

            case 'time of estimate':
                $propName = 'estimateTime';
                break;

            case 'terms and conditions':
                $propName = 'terms';
                break;

            case 'terms_conditions':
                $propName = 'terms';
                break;

            case 'terms':
                $propName = 'terms';
                break;

            case 'questions and comments':
                $propName = 'comments';
                break;

            case 'questions_comments':
                $propName = 'comments';
                break;

            case 'questions or comments':
                $propName = 'comments';
                break;

            case 'opt in for offers and information':
                $propName = 'optin';
                break;

            case 'opt_in':
                $propName = 'optin';
                break;

            case 'referring url':
                $propName = 'referer';
                break;

            case 'refurl':
                $propName = 'referer';
                break;

            case 'url':
                $propName = 'referer';
                break;

            case 'install date':
                $propName = 'installDate';
                break;

            case 'install time':
                $propName = 'installTime';
                break;

            case 'number of rooms':
                $propName = 'roomCount';
                break;

            case 'number_of_rooms':
                $propName = 'roomCount';
                break;

            case 'delivery date':
                $propName = 'deliveryDate';
                break;

            case 'material product code':
                $propName = 'productCode';
                break;

            case 'order date':
                $propName = 'invoiceDate';
                break;

            case 'product interest':
                $propName = 'productInterest';
                break;

            case 'interested_in':
                $propName = 'productInterest';
                break;

            case 'product_interest':
                $propName = 'productInterest';
                break;

            case 'product_choice':
                $propName = 'productInterest';
                break;

            case 'product type':
                $propName = 'productInterest';
                break;

            case 'product category':
                $propName = 'productInterest';
                break;

            case 'service code':
                $propName = 'serviceCode';
                break;

            case 'source':
                $propName = 'source';
                break;

            case 'project details':
                $propName = 'projectDetails';
                break;

            case 'project_details':
                $propName = 'projectDetails';
                break;

            case 'sales representative':
                $propName = 'salesRep';
                break;

            case 'store number':
                $propName = 'storeNumber';
                break;

            case 'would you like a flooring specialist to contact you?':
                $propName = 'contactMe';
                break;

            case 'what service are you interested in?':
                $propName = 'serviceInterest';
                break;

            case 'service_required':
                $propName = 'serviceInterest';
                break;

            case 'ss trigger':
                $propName = 'trigger';
                break;

            case 'trigger':
                $propName = 'trigger';
                break;

            case 'carpet cleaning date':
                $propName = 'carpetCleaningDate';
                break;

            case 'carpet cleaning time':
                $propName = 'carpet_cleaning_time';
                break;

            case 'stairs cleaned?':
                $propName = 'carpetCleaningStairs';
                break;
        }

        if (null === $propName) {
            write_log('field not defined in track-leads: ' . $lc_label);
        } else {
            $lead[$propName] = trim($value);
        }
    }
}

if (!function_exists('write_log')) {
    function write_log($log) {
        if (is_array($log) || is_object($log)) {
            error_log(print_r($log, true));
        } else {
            error_log($log);
        }
    }
}



